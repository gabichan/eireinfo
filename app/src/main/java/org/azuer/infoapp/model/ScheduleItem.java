package org.azuer.infoapp.model;

/**
 * Schedule item definition
 */

public class ScheduleItem {

    private String number;
    private String type;
    private String speciality;
    private String image;
    private String background;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
    public String getBackground()
    {
        return this.background;
    }
    public void setBackground(String background) {
        this.background=background;
    }
}
