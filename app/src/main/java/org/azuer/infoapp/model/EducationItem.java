package org.azuer.infoapp.model;

/**
 * Bean representing education level
 */
public class EducationItem {
    private String name;
    private String action;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
