package org.azuer.infoapp;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;

import org.azuer.infoapp.model.ScheduleItem;

/**
 * Fragment implementing dynamic list of schedules.
 *
 * Activity containing this fragment must implement
 * {@link ScheduleListFragment.OnScheduleFragmentIterationListener}.
 *
 * @author gabichan
 */
public class ScheduleListFragment extends Fragment {


    protected String type;// indica el tipo de horario a mostrar siendo ALL = mostrar todos
    /**
     * Listener attending changes in this fragment
     */
    private OnScheduleFragmentIterationListener mListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_schedule_list, container, false);

        if (savedInstanceState == null) {
            final Activity activity = getActivity();
            final RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);
            recyclerView.setLayoutManager(new GridLayoutManager(activity, 2));

            // TODO load list dynamically from uri
            recyclerView.setAdapter(new ScheduleListAdapter(getSchedulesDataset(type)));
        }

        return view;
    }

    /**
     * Gets schedules dataset
     *
     * @return {@link ScheduleItem} array
     */
    private ScheduleItem[] getSchedulesDataset(String education) {
        final ScheduleItem[] dataset;
        String fondo="@color/red";

        switch(education) {
            case "E.S.O":
            default: {
                dataset = new ScheduleItem[13];
                dataset[0] = buildScheduleItem("1º A", "E.S.O.", "", "E1A.jpg",fondo);
                dataset[1] = buildScheduleItem("1º B", "E.S.O.", "", "E1B.jpg",fondo);
                dataset[2] = buildScheduleItem("1º C", "E.S.O.", "", "E1C.jpg",fondo);
                dataset[3] = buildScheduleItem("2º A", "E.S.O.", "", "E2A.jpg",fondo);
                dataset[4] = buildScheduleItem("2º B", "E.S.O.", "", "E2B.jpg",fondo);
                dataset[5] = buildScheduleItem("2º C", "E.S.O.", "", "E2C.jpg",fondo);
                dataset[6] = buildScheduleItem("2º D", "E.S.O.", "", "E2D.jpg",fondo);
                dataset[7] = buildScheduleItem("2º", "E.S.O.", "PMAR", "E2PMAR.jpg",fondo);
                dataset[8] = buildScheduleItem("3º A", "E.S.O.", "", "E3A.jpg",fondo);
                dataset[9] = buildScheduleItem("3º B", "E.S.O.", "", "E3B.jpg",fondo);
                dataset[10] = buildScheduleItem("3º", "E.S.O.", "PMAR", "E3PMAR.jpg",fondo);
                dataset[11] = buildScheduleItem("4º", "E.S.O.", "Académicas", "E4Acad.jpg",fondo);
                dataset[12] = buildScheduleItem("4º", "E.S.O.", "Aplicadas", "E4Apli.jpg",fondo);
                break;
            }
            case "Bachillerato": {
                dataset = new ScheduleItem[4];
                dataset[0] = buildScheduleItem("1º", "Bachillerato", "Científico y tecnoloógico", "BCT1.jpg",fondo);
                dataset[1] = buildScheduleItem("2º", "Bachillerato", "Científico y tecnoloógico", "BCT2.jpg",fondo);
                dataset[2] = buildScheduleItem("1º", "Bachillerato", "Humanidades y CC. SS.", "BHCS1.jpg",fondo);
                dataset[3] = buildScheduleItem("2º", "Bachillerato", "Humanidades y CC. SS.", "BHCS2.jpg",fondo);
                break;
            }
            case "G.M.": {
                dataset = new ScheduleItem[8];
                dataset[0] = buildScheduleItem("1º", "Grado Medio", "Gestión Administrativa", "CM1A.jpg",fondo);
                dataset[1] = buildScheduleItem("1º", "Grado Medio", "Instalaciones Eléctricas y Automáticas", "CM1E.jpg",fondo);
                dataset[2] = buildScheduleItem("1º", "Grado Medio", "Instalaciones Frigoríficas y Climatización", "CM1F.jpg",fondo);
                dataset[3] = buildScheduleItem("1º", "Grado Medio", "Mecanizado", "CM1M.jpg",fondo);
                dataset[4] = buildScheduleItem("2º", "Grado Medio", "Gestión Administrativa", "CM2A.jpg",fondo);
                dataset[5] = buildScheduleItem("2º", "Grado Medio", "Instalaciones Eléctricas y Automáticas", "CM2E.jpg",fondo);
                dataset[6] = buildScheduleItem("2º", "Grado Medio", "Instalaciones Frigoríficas y Climatización", "CM2F.jpg",fondo);
                dataset[7] = buildScheduleItem("2º", "Grado Medio", "Mecanizado", "CM2M.jpg",fondo);
                break;
            }
            case "G.S.": {
                dataset = new ScheduleItem[6];
                dataset[0] = buildScheduleItem("1º", "Grado Superior", "Administración y Finanzas", "CS1A.jpg",fondo);
                dataset[1] = buildScheduleItem("1º", "Grado Superior", "DEPORTES", "CS1D.jpg",fondo);
                dataset[2] = buildScheduleItem("2º", "Grado Superior", "Administración y Finanzas", "CS2A.jpg",fondo);
                dataset[3] = buildScheduleItem("2º", "Grado Superior", "DEPORTES", "CS2D.jpg",fondo);
                dataset[4] = buildScheduleItem("1º", "FPB", "Fabricación y Montaje", "FPB1.jpg",fondo);
                dataset[5] = buildScheduleItem("2º", "FPB", "Fabricación y Montaje", "FPB2.jpg",fondo);
                break;
            }
        }
        return dataset;
    }

    private ScheduleItem buildScheduleItem(final String number, final String type, final String specialty, final String image, final String fondo) {
        final ScheduleItem item = new ScheduleItem();

        item.setNumber(number);
        item.setType(type);
        item.setSpeciality(specialty);
        item.setImage("file:///android_asset/schedules/" + image);
        item.setBackground(fondo);

        return item;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnScheduleFragmentIterationListener) {
            mListener = (OnScheduleFragmentIterationListener) context;
            mListener.reset();
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnScheduleFragmentIterationListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (mListener != null) {
            mListener.reset();
        }
    }

    public interface OnScheduleFragmentIterationListener {
        void onCardClicked(String classNumber);

        void reset();
    }
}
