package org.azuer.infoapp;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.azuer.infoapp.model.BookItem;

/**
 * Fragment implementing dynamic list of books.
 *
 * Activity containing this fragment must implement
 * {@link BookListFragment.OnBookFragmentIterationListener}.
 *
 * @author gabichan
 */
public class BookListFragment extends Fragment {


    protected String type;// indica el tipo de horario a mostrar siendo ALL = mostrar todos
    /**
     * Listener attending changes in this fragment
     */
    private OnBookFragmentIterationListener mListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_book_list, container, false);

        if (savedInstanceState == null) {
            final Activity activity = getActivity();
            final RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);
            recyclerView.setLayoutManager(new GridLayoutManager(activity, 2));

            // TODO load list dynamically from uri
            recyclerView.setAdapter(new BookListAdapter(getBooksDataset(type)));
        }

        return view;
    }

    /**
     * Gets books dataset
     *
     * @return {@link BookItem} array
     */
    private BookItem[] getBooksDataset(String education) {
        final BookItem[] dataset;
        final String fondo="@color/red";

        switch(education) {
            case "E.S.O":
            default: {
                dataset = new BookItem[5];
                dataset[0] = buildBookItem("1º", "E.S.O.", "", "ESO1.jpg",fondo);
                dataset[1] = buildBookItem("2º", "E.S.O.", "", "ESO2.jpg",fondo);
                dataset[2] = buildBookItem("3º", "E.S.O.", "", "ESO3.jpg",fondo);
                dataset[3] = buildBookItem("4º", "E.S.O.", "Académicas", "ESO4Acad.jpg",fondo);
                dataset[4] = buildBookItem("4º", "E.S.O.", "Aplicadas", "ESO4Apli.jpg",fondo);
                break;
            }

            case "Bachillerato": {
                dataset = new BookItem[4];
                dataset[0] = buildBookItem("1º", "Bachillerato", "Científico y tecnoloógico", "BCT1.jpg",fondo);
                dataset[1] = buildBookItem("2º", "Bachillerato", "Científico y tecnoloógico", "BCT2.jpg",fondo);
                dataset[2] = buildBookItem("1º", "Bachillerato", "Humanidades y CC. SS.", "BHCS1.jpg",fondo);
                dataset[3] = buildBookItem("2º", "Bachillerato", "Humanidades y CC. SS.", "BHCS2.jpg",fondo);
                break;
            }

            case "G.M.": {
                dataset = new BookItem[8];
                dataset[0] = buildBookItem("1º", "Grado Medio", "Gestión Administrativa", "nodisponible.jpg",fondo);
                dataset[1] = buildBookItem("1º", "Grado Medio", "Instalaciones Eléctricas y Automáticas", "nodisponible.jpg",fondo);
                dataset[2] = buildBookItem("1º", "Grado Medio", "Instalaciones Frigoríficas y Climatización", "nodisponible.jpg",fondo);
                dataset[3] = buildBookItem("1º", "Grado Medio", "Mecanizado", "nodisponible.jpg",fondo);
                dataset[4] = buildBookItem("2º", "Grado Medio", "Gestión Administrativa", "nodisponible.jpg",fondo);
                dataset[5] = buildBookItem("2º", "Grado Medio", "Instalaciones Eléctricas y Automáticas", "nodisponible.jpg",fondo);
                dataset[6] = buildBookItem("2º", "Grado Medio", "Instalaciones Frigoríficas y Climatización", "nodispoible.jpg",fondo);
                dataset[7] = buildBookItem("2º", "Grado Medio", "Mecanizado", "CM2M.jpg",fondo);
                break;
            }

            case "G.S.": {
                dataset = new BookItem[6];
                dataset[0] = buildBookItem("1º", "Grado Superior", "Administración y Finanzas", "nodisponible.jpg",fondo);
                dataset[1] = buildBookItem("1º", "Grado Superior", "DEPORTES", "nodisponible.jpg",fondo);
                dataset[2] = buildBookItem("2º", "Grado Superior", "Administración y Finanzas", "nodisponible.jpg",fondo);
                dataset[3] = buildBookItem("2º", "Grado Superior", "DEPORTES", "CS2D.jpg",fondo);
                dataset[4] = buildBookItem("1º", "FPB", "Fabricación y Montaje", "nodisponible.jpg",fondo);
                dataset[5] = buildBookItem("2º", "FPB", "Fabricación y Montaje", "nodisponible.jpg",fondo);
                break;
            }
        }
        return dataset;
    }

    private BookItem buildBookItem(final String number, final String type, final String specialty, final String image, final String fondo) {
        final BookItem item = new BookItem();

        item.setNumber(number);
        item.setType(type);
        item.setSpeciality(specialty);
        item.setImage("file:///android_asset/books/" + image);
        item.setBackground(fondo);

        return item;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnBookFragmentIterationListener) {
            mListener = (OnBookFragmentIterationListener) context;
            mListener.reset();
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnBookFragmentIterationListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (mListener != null) {
            mListener.reset();
        }
    }

    public interface OnBookFragmentIterationListener {
        void onCardClicked(String classNumber);

        void reset();
    }
}
