package org.azuer.infoapp;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.azuer.infoapp.model.BookItem;


/**
 * Adapter for list of card views representing available schedules.
 *
 * @author gabichan
 */
class BookListAdapter extends RecyclerView.Adapter<BookListAdapter.ViewHolder> {

    /**
     * Set of data to generate the list
     */
    private BookItem [] dataset;

    static class ViewHolder extends RecyclerView.ViewHolder {

        /**
         * {@link TextView} displaying class number
         */
        private TextView textNumber;

        private TextView textType;
        private TextView textSpecility;
        private LinearLayout contenedor;

        private String image;

        ViewHolder(final Context context, CardView v) {
            super(v);

            textNumber = (TextView)v.findViewById(R.id.book_item_number);
            textType = (TextView)v.findViewById(R.id.book_item_type);
            textSpecility = (TextView)v.findViewById(R.id.book_item_speciality);
            contenedor=(LinearLayout) v.findViewById(R.id.book_contend);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MainActivity) context).onCardClicked("1A");
                    ((MainActivity) context).showBookItem(image);
                }
            });
        }
    }

    /**
     * Constructor
     *
     * @param dataset    set of data
     *
     * TODO load data set internally
     */
    BookListAdapter(BookItem[]dataset) {
        this.dataset = dataset;
    }

    @Override
    public BookListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_book_item, parent, false);

        // set the view's size, margins, paddings and layout parameters

        return new ViewHolder(parent.getContext(), (CardView)v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
          holder.textNumber.setText(dataset[position].getNumber());
        holder.textType.setText(dataset[position].getType());
        holder.textSpecility.setText(dataset[position].getSpeciality());
        holder.image = dataset[position].getImage();


    }

    @Override
    public int getItemCount() {
        return dataset.length;
    }
}
