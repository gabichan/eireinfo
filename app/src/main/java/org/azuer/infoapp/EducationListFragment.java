package org.azuer.infoapp;

import android.app.Activity;
import android.content.Context;
        import android.os.Bundle;
        import android.support.annotation.Nullable;
        import android.support.v4.app.Fragment;
        import android.support.v7.widget.GridLayoutManager;
        import android.support.v7.widget.RecyclerView;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
import org.azuer.infoapp.model.EducationItem;

/**
 * List of education levels to choose one
 */
public class EducationListFragment extends Fragment {


    /**
     * Fragment implementing dynamic list of schedules.
     *
     * Activity containing this fragment must implement
     * {@link EducationListFragment.OnEducationFragmentIterationListener}.
     *
     * @author mauricio
     */


        /**
         * Listener attending changes in this fragment
         */
        private OnEducationFragmentIterationListener mListener;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            final View view = inflater.inflate(R.layout.fragment_education_list, container, false);

            if (savedInstanceState == null) {
                final Activity activity = getActivity();
                final RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);

                recyclerView.setLayoutManager(new GridLayoutManager(activity,2));
               // recyclerView.getLayoutManager():
                // TODO load list dynamically from uri
                recyclerView.setAdapter(new EducationListAdapter(getEducationDataset(),MainActivity.btn_click));




            }

            return view;
        }

        /**
         * Gets schedules dataset
         *
         * @return {@link EducationItem} array
         */
        private EducationItem[] getEducationDataset() {
            final EducationItem[] dataset = new EducationItem[4];


            dataset[0] = buildEducationItem( "E.S.O.", "E.S.O");
            dataset[1] = buildEducationItem("BACHILLERATO","Bachillerato");
            dataset[2] = buildEducationItem("GRADO MEDIO", "G.M.");
            dataset[3] = buildEducationItem("GRADO SUPERIOR","G.S.");

            return dataset;
        }

        private EducationItem buildEducationItem(final String name, final String action) {
            final EducationItem item = new EducationItem();


            item.setName(name);
            item.setAction(action);
           // item.setImage("file:///android_asset/schedules/" + image);


            return item;
        }

        @Override
        public void onAttach(Context context) {
            super.onAttach(context);
            if (context instanceof OnEducationFragmentIterationListener) {
                mListener = (OnEducationFragmentIterationListener) context;
                mListener.reset();
            } else {
                throw new RuntimeException(context.toString()
                        + " must implement OnEducationFragmentIterationListener");
            }
        }

        @Override
        public void onDetach() {
            super.onDetach();
            mListener = null;
        }

        @Override
        public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
            super.onViewStateRestored(savedInstanceState);
            if (mListener != null) {
                mListener.reset();
            }
        }

        public interface OnEducationFragmentIterationListener {
            void onCardClicked(String classNumber);

            void reset();
        }
    }





