package org.azuer.infoapp;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import org.azuer.infoapp.helpers.CustomTabActivityHelper;
import org.azuer.infoapp.helpers.WebviewFallback;

/**
 * Schedule section's activity
 *
 * It manages fragments to show specific content.
 *
 * Implements {@link ScheduleListFragment.OnScheduleFragmentIterationListener}
 * to interact with list of schedules.
 *
 * @author gabichan
 */
public class MainActivity extends AppCompatActivity
        implements ScheduleListFragment.OnScheduleFragmentIterationListener,EducationListFragment.OnEducationFragmentIterationListener,
        BookListFragment.OnBookFragmentIterationListener {

    /**
     * Back stack names
     */
    private enum BackStackStepNames { DETAILS, LIST, MAIN }

    /**
     *
     */
    public static final String ESO="E.S.O";
    /**
     * List of contact emails
     */
    private static final String[] CONTACT_EMAIL = {"azuerjefatura3@gmail.com"};

    /**
     * URL to dial IES Azuer number
     */
    private static final String URL_CONTACT_PHONE = "tel:+34926610524";

    /**
     * URL to open Facebook profile with Facebook app
     */
    private static final String URL_FB_APP = "fb://profile/iesazuer";

    /**
     * URL for generic http Facebook profile
     */
    private static final String URL_FB_HTTP = "https://www.facebook.com/iesazuer";

    /**
     * URL for map location
     */
    private static final String URL_LOCATION = "https://www.google.es/maps/place/Instituto+de+Educaci%C3%B3n+Secundaria+Ies+Azuer/@38.995336,-3.3623356,17z/data=!4m12!1m6!3m5!1s0xd69144165ee450f:0x89f0bacf114bad4!2sInstituto+de+Educaci%C3%B3n+Secundaria+Ies+Azuer!8m2!3d38.995336!4d-3.3601469!3m4!1s0xd69144165ee450f:0x89f0bacf114bad4!8m2!3d38.995336!4d-3.3601469";

    /**
     * URL to open Twitter profile with Twitter app
     */
    private static final String URL_TWITTER_APP = "twitter://user?screen_name=iazuer";

    /**
     * URL for generic http Twitter profile
     */
    private static final String URL_TWITTER_HTTP = "https://www.twitter.com/iazuer";

    /**
     * URL of IES Azuer activities
     */
    private static final String URL_WEB_ACTIVITIES = "http://www.iesazuer.es/actividades-extraescolares/";

    /**
     * URL of IES Azuer web home
     */
    private static final String URL_WEB_HOME = "http://www.iesazuer.es/";

    /**
     * URL of IES Azuer news
     */
    private static final String URL_WEB_NEWS = "http://www.iesazuer.es/noticias";

    /**
     * URL of IES Azuer Youtube channel
     */
    private static final String URL_YOUTUBE = "https://www.youtube.com/channel/UCzPfhllakfSs5bk7eYwqCBA";

    // TODO check if this value is needed
    private String itemDetailLoaded;

    private String textopulsado;
    public static  String btn_click;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // TODO search use for floating action button
        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "No hace nada. Buscar utilidad.", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        if (savedInstanceState == null && findViewById(R.id.fragment_container) != null) {
            final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.fragment_container, new MainMenuFragment(), BackStackStepNames.MAIN.name());
            transaction.commit();
        }
    }
    /**
     * Replaces list fragment by details fragment of choose option
     *
     * @param actionbefore   image url
     *                    por REVISAR
     */
    public void choiceEducationItem(final String actionbefore) {
        // create details fragment
        this.textopulsado=(actionbefore);

    }
    /**
     * Replaces list fragment by details fragment of choose option
     *
     * @param imageUrl    image url
     */
    public void showBookItem(final String imageUrl) {
        // create details fragment
        final BookDetailFragment bookItem = BookDetailFragment.newInstance(imageUrl);

        // make transaction
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        transaction.replace(R.id.fragment_container, bookItem);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.addToBackStack(BackStackStepNames.DETAILS.name());
        transaction.commit();
    }

    /**
     * Replaces list fragment by details fragment of choose option
     *
     * @param imageUrl    image url
     */
    public void showScheduleItem(final String imageUrl) {
        // create details fragment
        final ScheduleDetailFragment scheduleItem = ScheduleDetailFragment.newInstance(imageUrl);

        // make transaction
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, scheduleItem);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.addToBackStack(BackStackStepNames.DETAILS.name());
        transaction.commit();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // TODO check this
        outState.putString("item_detail_loaded", itemDetailLoaded);
    }

    @Override
    public void onCardClicked(String classNumber) {
        this.itemDetailLoaded = classNumber;
    }

    @Override
    public void reset() {
        this.itemDetailLoaded = null;
    }

    public void dialPhone(View view) {
        final Intent intet = new Intent(Intent.ACTION_DIAL, Uri.parse(URL_CONTACT_PHONE));
        startActivity(intet);
    }

    public void sendMail(View view) {
        final Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_EMAIL, CONTACT_EMAIL);
        startActivity(intent);
    }

    /**
     * Opens Youtube channel using Youtube app if it exists. Otherwise, it opens a
     * Chrome's custom tab.
     *
     * @param view    {@link View}
     */
    public void viewYoutubeChannel(View view) {
        Intent intent;
        try {
            intent =new Intent(Intent.ACTION_VIEW);
            intent.setPackage("com.google.android.youtube");
            intent.setData(Uri.parse(URL_YOUTUBE));
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            openCustomTab(URL_YOUTUBE);
        }
    }

    /**
     * Method fired when user clicks on {@link R.id#btn_facebook} button.
     *
     * Opens IES Azuer's Facebook profile.
     *
     * @param view    {@link View}
     */
    public void viewFacebookProfile(View view) {
        openBestApp(URL_FB_APP, URL_FB_HTTP);
    }

    /**
     * Method fired when user clicks on {@link R.id#btn_twitter} button.
     *
     * Opens IES Azuer's Twitter profile.
     *
     * @param view    {@link View}
     */
    public void viewTwitterProfile(View view) {
        openBestApp(URL_TWITTER_APP, URL_TWITTER_HTTP);
    }

    /**
     * This method is fired when user cliks on {@link R.id#btn_location} button.
     *
     * Opens Googe Maps.
     *
     * @param view    {@link View}
     */
    public void viewLocation(View view) {
        openBestApp(URL_LOCATION, URL_LOCATION);
    }

    /**
     * Method fired when user clicks on {@link R.id#btn_activities} button.
     *
     * It launches a Chrome's custom tab loading url {@link #URL_WEB_ACTIVITIES}
     *
     * @param view    {@link View}
     */
    public void viewActivities(View view) {
        openCustomTab(URL_WEB_ACTIVITIES);
    }

    /**
     * Method fired when user clicks on {@link R.id#btn_web} button.
     *
     * It launches a Chrome's custom tab loading url {@link #URL_WEB_HOME}
     *
     * @param view    {@link View}
     */
    public void viewHome(View view) {
        openCustomTab(URL_WEB_HOME);
    }

    /**
     * Method fired when user clicks on {@link R.id#btn_news} button.
     *
     * It launches a Chrome's custom tab loading url {@link #URL_WEB_NEWS}
     *
     * @param view    {@link View}
     */
    public void viewNews(View view) {
        openCustomTab(URL_WEB_NEWS);
    }

   /**
            * @param view
    */
    public void viewEducationsBook(View view)
    {
        // create details fragment
        btn_click="btn_books";
        final EducationListFragment educationList = new EducationListFragment();

        // make transaction
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        //ImageButton button1 = (ImageButton)findViewById(R.id.btn_books);
        transaction.replace(R.id.fragment_container, educationList);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.addToBackStack(BackStackStepNames.LIST.name());

        transaction.commit();

        //getActionBar().setIcon(R.drawable.my_icon);;

    }
/**
 * @param view
 */
    public void viewEducations(View view)
    {
        btn_click="btn_schedule";
        // create details fragment
        final EducationListFragment educationList = new EducationListFragment();
        // make transaction
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, educationList);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.addToBackStack(BackStackStepNames.LIST.name());
        transaction.commit();
    }
    /**
     * Method fired when user clicks on {@link R.id#btn_schedules} button.
     *
     * Opens list of courses to select schedule details.
     *
     * @param view
     */

    public void viewBooks(View view) {
        // create details fragment
        final BookListFragment bookList = new BookListFragment();
        bookList.type = textopulsado;

        // make transaction
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        transaction.replace(R.id.fragment_container, bookList);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.addToBackStack(BackStackStepNames.LIST.name());
        transaction.commit();

    }
    /**
     * Method fired when user clicks on {@link R.id#btn_schedules} button.
     *
     * Opens list of courses to select schedule details.
     *
     * @param view
     */

    public void viewSchedules(View view) {
        // create details fragment
        final ScheduleListFragment scheduleList = new ScheduleListFragment();
        scheduleList.type = textopulsado;

        // make transaction
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, scheduleList);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.addToBackStack(BackStackStepNames.LIST.name());
        transaction.commit();
    }

    /**
     * This method queries {@link PackageManager} to check if exists an app to open the
     * specific url. Otherwise, a app is choosed to open http url.
     *
     * @param specificUrl    specific url for app
     * @param httpUrl        generic http url
     */
    private void openBestApp(final String specificUrl, final String httpUrl) {
        final PackageManager manager = this.getPackageManager();
        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(specificUrl));
        if (manager.queryIntentActivities(intent, 0).size() > 0) {
            startActivity(intent);
        } else {
            intent.setData(Uri.parse(httpUrl));
            startActivity(Intent.createChooser(intent, getResources().getString(R.string.openWith)));
        }
    }

    /**
     * Private method to build a new Chrome's custom tab an load url defined by parameter.
     *
     * @param url    url to open
     */
    private void openCustomTab(String url) {
        //final Bitmap closeIcon = BitmapFactory.decodeResource(this.getApplicationContext().getResources(), R.drawable.ic_arrow_back_white_24dp);
        //final Bitmap shareIcon = BitmapFactory.decodeResource(this.getApplicationContext().getResources(), R.drawable.ic_share_white_24dp);
        final Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("cardText/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, url);

        final CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
        builder.setToolbarColor(getResources().getColor(R.color.colorPrimary));
        builder.addDefaultShareMenuItem();
        builder.setShowTitle(true);
        //builder.setCloseButtonIcon(closeIcon);
        //builder.setActionButton(shareIcon, "compartir", PendingIntent.getBroadcast(getApplicationContext(), 0, shareIntent, 0), false);
        final CustomTabsIntent customTabsIntent = builder.build();

        CustomTabActivityHelper.openCustomTab(
                this,
                customTabsIntent,
                Uri.parse(url),
                new WebviewFallback()
        );
    }
}
