package org.azuer.infoapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import uk.co.senab.photoview.PhotoViewAttacher;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EducationDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 * @author gabichan
 */
public class EducationDetailFragment extends Fragment {
    private static final String ARG_IMG_URL = "param1";

    private String itemUrl;

    private PhotoViewAttacher phAttatcher;

    //private OnScheduleFragmentIterationListener mListener;

    public EducationDetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param itemUrl Parameter 1.
     * @return A new instance of fragment ScheduleDetailFragment.
     */
    public static EducationDetailFragment newInstance(final String itemUrl) {
        EducationDetailFragment fragment = new EducationDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_IMG_URL, itemUrl);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            itemUrl = getArguments().getString(ARG_IMG_URL);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_education_detail, container, false);
// REVISAR TIENE QUE ABRIR SCHEDULE
        if (savedInstanceState == null) {
            final ImageView img = (ImageView) view.findViewById(R.id.education_image);
            Glide.with(this).load(itemUrl).into(img);

            this.phAttatcher = new PhotoViewAttacher(img);
        }

        phAttatcher.update();

        return view;
    }
}
