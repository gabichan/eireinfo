package org.azuer.infoapp;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.azuer.infoapp.model.EducationItem;

/**
 * List adapter for education level choice
 */
class EducationListAdapter extends RecyclerView.Adapter<EducationListAdapter.ViewHolder>
{

    /**
     * Set of data to generate the list
     */
    private EducationItem[] dataset;

    private String btn_onclick;

    static class ViewHolder extends RecyclerView.ViewHolder {

        /**
         * {@link TextView} displaying class number
         */
        private TextView textName;


        private String action;

        ViewHolder(final Context context, final CardView v, final String btn_onclick) {
            super(v);

            textName = (TextView)v.findViewById(R.id.education_item_type);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MainActivity) context).choiceEducationItem(action);

                 if (btn_onclick.equals("btn_books"))
                 {
                     ((MainActivity) context).viewBooks(view);
                 }else  ((MainActivity) context).viewSchedules(view);

                }
            });

        }
    }

    /**
     * Constructor
     *
     * @param dataset    set of data
     *
     * TODO load data set internally
     */
    EducationListAdapter(EducationItem[] dataset,String btn_onclick) {
        this.dataset = dataset;
        this.btn_onclick=btn_onclick;
    }

    @Override
    public EducationListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
       
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_education_item, parent, false);

        // set the view's size, margins, paddings and layout parameters

        return new EducationListAdapter.ViewHolder(parent.getContext(),(CardView)v,btn_onclick);
    }


    @Override
    public void onBindViewHolder(EducationListAdapter.ViewHolder holder, int position) {
        holder.textName.setText(dataset[position].getName());

        holder.action = dataset[position].getAction();


    }

    @Override
    public int getItemCount() {
        return dataset.length;
    }
}


